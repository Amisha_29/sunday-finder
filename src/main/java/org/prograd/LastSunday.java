package org.prograd;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class LastSunday {

    public String[] of(int year) {

        if(countDigitsInYear(year)!=4){
            throw new IllegalArgumentException("Year should contain four Digits.");
        }
        if(year<0){
            throw new IllegalArgumentException("Year should be a Positive Number.");
        }

        boolean isLeap = isLeapYear(year);
        int[] days = {31, isLeap ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        String[] lastSundayList = new String[12];

        for (int month = 1, index = 0; month <= 12; month++, index++) {
            int date;
            for (date = days[index]; getWeekDay(year, month, date) != 7; date--)
                ;
            lastSundayList[index] = String.valueOf(year) + "-" + String.valueOf(month) + "-" + String.valueOf(date);
        }

        return lastSundayList;
    }

    private static int countDigitsInYear(int year){
        int countDigits = 0;
        while(year != 0){
            countDigits++;
            year = year/10;
        }
        return countDigits;
    }

    private static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 != 0)
                return true;
            else if (year % 400 == 0)
                return true;
        }
        return false;
    }

    private static int getWeekDay(int year, int month, int day) {

        LocalDate localDate = LocalDate.of(year, month, day);
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();

        return dayOfWeek.getValue();
    }

}
