package org.prograd;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LastSundayTest {
    LastSunday lastSunday = new LastSunday();

    @Test
    void shouldReturnLastSundayOfEachMonthWhenYearIsGiven(){

        String[] actualResult = lastSunday.of(2013);
        String[] expectedResult = new String[]{"2013-1-27", "2013-2-24", "2013-3-31", "2013-4-28",
                "2013-5-26", "2013-6-30", "2013-7-28", "2013-8-25", "2013-9-29", "2013-10-27",
                "2013-11-24", "2013-12-29"};

        assertThat(actualResult,is(equalTo(expectedResult)));
    }

    @Test
    void shouldReturnLastSundayOfEachMonthWhenLeapYearIsGiven(){

        String[] actualResult = lastSunday.of(2020);
        String[] expectedResult = new String[]{"2020-1-26", "2020-2-23", "2020-3-29", "2020-4-26",
                "2020-5-31", "2020-6-28", "2020-7-26", "2020-8-30", "2020-9-27", "2020-10-25",
                "2020-11-29", "2020-12-27"};
        assertThat(actualResult,is(equalTo(expectedResult)));
    }

    @Test
    void shouldThrowExceptionWhenGivenYearDoesNotContainFourDigits(){
        assertThrows(IllegalArgumentException.class, ()->lastSunday.of(567));
    }

    @Test
    void shouldThrowExceptionWhenGiveYearIsNegative(){
        assertThrows(IllegalArgumentException.class, ()->lastSunday.of(-2012));
    }

}
